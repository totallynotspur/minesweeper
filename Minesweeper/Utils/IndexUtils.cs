﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Minesweeper.Utils
{
    public static class IndexUtils
    {
        public enum MinePatchClasses
        {
            mined,
            cleared,
            mystery,
            mysteryMined,
        }
    
        public static string CalculatePatchColour(bool cleared, bool mined, bool gameOver)
        {
            string output = null;

            if (mined)
            {
                if (cleared)
                {
                    output = MinePatchClasses.mined.ToString();
                }
                else if (gameOver)
                {
                    output = MinePatchClasses.mysteryMined.ToString();
                }
                else
                {
                    output = MinePatchClasses.mystery.ToString();
                }
            }
            else if (cleared)
            {
                output = MinePatchClasses.cleared.ToString();
            }
            else
            {
                output = MinePatchClasses.mystery.ToString();
            }

            return output;
        }
    }
}