﻿using System;
using System.Collections.Generic;

namespace Minesweeper.Models
{
    public class Minefield : List<List<MinePatch>>
    {
        public bool GameOver { get; protected set; }
        public int Width { get; protected set; }
        public int Height { get; protected set; }

        public static Minefield NewBoard(int width, int height)
        {
            var output = new Minefield() { Width = width, Height = height };
            Random r = new Random();
            for (int x = 0; x < width; x++)
            {
                var currentRow = new List<MinePatch>();
                output.Add(currentRow);
                for (int y = 0; y < width; y++)
                {
                    var mined = r.Next(0, 99) <= 15;
                    currentRow.Add(new MinePatch() { Cleared = false, Mined = mined, X = x, Y = y });
                }
            }
            return output;
        }

        public MinePatch ClearPatch(int x, int y)
        {
            var output = this[x][y];
            output.Cleared = true;
            if (output.Mined)
            {
                GameOver = true;
            }
            else
            {
                output.SurroundingMines = CountSurroundingMines(x, y);
                if (output.SurroundingMines == 0)
                {
                    ClearAround(x, y);
                }
            }
            return output;
        }

        private void ClearAround(int targetX, int targetY)
        {
            var clearX = 0;
            var clearY = 0;
            for (var x = -1; x <= 1; x++)
            {
                for (var y = -1; y <= 1; y++)
                {
                    clearX = targetX + x;
                    clearY = targetY + y;
                    if (ValidPatch(clearX, clearY) && !this[clearX][clearY].Cleared)
                    {
                        ClearPatch(clearX, clearY);
                    }
                }
            }                        
        }

        private int CountSurroundingMines(int targetX, int targetY)
        {
            var output = 0;
            var countX = 0;
            var countY = 0;
            for (var x = -1; x <= 1; x++)
            {
                for (var y = -1; y <= 1; y++)
                {
                    countX = targetX + x;
                    countY = targetY + y;
                    if (ValidPatch(countX, countY) && this[countX][countY].Mined)
                    {
                        output++;
                    }
                }
            }
            return output;
        }

        public bool ValidPatch(int x, int y)
        {
            var output = x >= 0 && x < Width && y >= 0 && y < Height;
            return output;
        }
    }
}