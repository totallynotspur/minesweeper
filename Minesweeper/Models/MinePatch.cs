﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Minesweeper.Models
{
    public class MinePatch
    {
        public bool Mined { get; set; }
        public bool Cleared { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int? SurroundingMines { get; set; }
    }
}
