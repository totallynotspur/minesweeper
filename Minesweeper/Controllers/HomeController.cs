﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Minesweeper.Models;

namespace Minesweeper.Controllers
{
    public class HomeController : Controller
    {
        private Minefield GetMinefield()
        {
            var output = SessionBag.Current.Minefield ?? Minefield.NewBoard(10, 10);
            SessionBag.Current.Minefield = output;
            return output;
        }

        private void ResetMinefield()
        {
            SessionBag.Current.Minefield = Minefield.NewBoard(10, 10);
        }

        public ActionResult Index(int? x, int? y)
        {
            var minefield = GetMinefield();
            if (!minefield.GameOver)
            {
                if (x != null && y != null)
                {
                    minefield.ClearPatch((int)x, (int)y);
                }
            }

            return View(minefield);
        }

        public ActionResult Reset()
        {
            ResetMinefield();
            return RedirectToAction("Index");
        }
    }
}
